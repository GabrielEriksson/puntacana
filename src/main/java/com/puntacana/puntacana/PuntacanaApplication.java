package com.puntacana.puntacana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PuntacanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PuntacanaApplication.class, args);
	}

}

