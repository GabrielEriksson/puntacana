window.onload = function() {

    var activeImageIndex = 9;
    var nextImageIndex = 8;

    var startDirection = true;

    var start = true;

    var activeImage;
    var nextImage;

    var windowWidth = window.innerWidth;
    var image1 = $('.imgbox > img')[0].style.left = windowWidth + 'px';
    var image1 = $('.imgbox > img')[1].style.left = windowWidth + 'px';
    var image1 = $('.imgbox > img')[2].style.left = windowWidth + 'px';
    var image1 = $('.imgbox > img')[3].style.left = windowWidth + 'px';
    var image1 = $('.imgbox > img')[4].style.left = windowWidth + 'px';
    var image1 = $('.imgbox > img')[5].style.left = windowWidth + 'px';
    var image1 = $('.imgbox > img')[6].style.left = windowWidth + 'px';
    var image1 = $('.imgbox > img')[7].style.left = windowWidth + 'px';


    setInterval(function() {
            var regex = /.*images\/PuntaCana/gi;
            var activeImageClass = '.image-' + activeImageIndex;
            var nextImageClass = '.image-' + nextImageIndex;
            activeImage = $(activeImageClass);
            nextImage = $(nextImageClass);
            var source = activeImage.attr('src');
            var cutBefore = source.replace(regex, '');
            var index = cutBefore.replace(".jpg", '');
            if (start) {
                activeImage[0].style.left = 0 + 'px';
                start = false;
            }
            else {
                if (startDirection) {
                    activeImage[0].style.left = '-' + windowWidth + 'px';
                    nextImage[0].style.left = 0 + 'px';
                    activeImageIndex--;
                    nextImageIndex--;
                }
                else {
                    nextImage[0].style.left = windowWidth + 'px';
                    activeImage[0].style.left = 0 + 'px';
                    activeImageIndex++;
                    nextImageIndex++;
                }
            }
            if (nextImageIndex == 1) {
                startDirection = false;
            }
            else if (nextImageIndex == 8) {
                startDirection = true;
            }
    }, 5000);

    var date = new Date(2019, 4, 4);
    var now = new Date();
    var diff = (date.getTime()/1000) - (now.getTime()/1000);

    var clock = $('.your-clock').FlipClock(diff, {
        clockFace: 'DailyCounter',
        countdown: true
    });
    clock.start();

    setTimeout(function() {
        $('.audio-player')[0].play().catch(function() {});
    }, 1500);

}